ToyDroid
======================
A framework for Android apps analysis based on 
[WALA](http://wala.sourceforge.net/wiki/index.php/Main_Page). 
[Here](https://github.com/hjjandy/WALA)
provides a modified version which removes the Eclipse Plugin dependencies.


ToyDroid.BidText
======================
A static tool for detecting sensitive data disclosures 
in Android apps via Bi-directional Text correlation analysis.
